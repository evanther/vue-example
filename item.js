Vue.component('item', {
  props : {
    item : {
      type : Object,
      required : true
    }
  },
  data : function() {
    console.log("Render event for item " + this.item.id)
    return {
      internalData : {
        id : this.item.id,
        name : this.item.name,
        modified : this.item.modified
      }
    }
  },
  template : `
    <tr>
      <th><div>{{internalData.id}}</div></th>
      <th><div>{{internalData.name}}</div></th>
      <th><div>{{internalData.modified}}</div></th>
    </tr>
  `
})