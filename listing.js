Vue.component('listing', {
  template : `
    <div>
      <table border=1>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Modified</th>
        </tr>
        <item v-for="i in items" v-bind:item=i v-bind:key=i.id></item>
      </table>
      <button v-on:click=save>Save</button>
    </div>
  `,
  data : function() {
    return {
      items : [{
      	id : 1,
      	name : "Alfajor",
      	modified : true
      },{
      	id : 2,
      	name : "Pepas",
      	modified : false
      },{
      	id : 3,
      	name : "Cafe",
      	modified : false
      }]
    }
  },
  methods : {
    save : function() {
      console.log("Save clicked")
      
      // Opcion 1
      this.items[0].modified = false

      // Opcion 2: usar la magia de $set
	  /*
	  this.$set(this.items, 0, {
      	id : 1,
      	name : "Alfajor",
      	modified : false
      })
      */
      
	  // Opcion 3: reemplazar el objeto en el indice con splice
      /*
      this.items.splice(0, 1, {
      	id : 1,
      	name : "Alfajor",
      	modified : false
      })
      */

      // Opcion 4: eliminar el indice en cuestion y meter un objeto nuevo en el array
      /*
      this.items.splice(0, 1)
      this.items.push({
      	id : 1,
      	name : "Alfajor",
      	modified : false
      })
      */
    }
  }
})